use anyhow::Result;
use qdrant_client::prelude::*;
use qdrant_client::qdrant::{
    CreateCollection, PointStruct, SearchPoints, VectorParams, VectorsConfig
};
use qdrant_client::qdrant::vectors_config::Config;
use serde_json::json;
use std::convert::TryInto;

#[tokio::main]
async fn main() -> Result<()> {
    let client = initialize_qdrant_client().await?;

    let collection_name = "test_collection";
    create_collection(&client, collection_name).await?;
    ingest_vectors(&client, collection_name).await?;
    query_applicants(&client, collection_name).await?;

    Ok(())
}

async fn initialize_qdrant_client() -> Result<QdrantClient> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;
    Ok(client)
}

async fn create_collection(client: &QdrantClient, collection_name: &str) -> Result<()> {
    // Delete collection if it already exists
    let _ = client.delete_collection(collection_name).await;

    // Create collection with desired configuration
    client.create_collection(&CreateCollection {
        collection_name: collection_name.into(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 3,
                distance: Distance::Cosine.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;

    Ok(())
}



async fn ingest_vectors(client: &QdrantClient, collection_name: &str) -> Result<()> {
    // Create 10 vectors
    let points = vec![
        PointStruct::new(
            1,
            vec![20.0, 168.0, 50.0], // Convert integers to floats
            json!({"name": "Cindy"}).try_into().unwrap(),
        ),
        PointStruct::new(
            2,
            vec![35.0, 187.0, 70.0], // Convert integers to floats
            json!({"name": "Joey"}).try_into().unwrap(),
        ),
        PointStruct::new(
            3,
            vec![70.0, 170.0, 80.0], // Convert integers to floats
            json!({"city": "Alex"}).try_into().unwrap(),
        ),
    ];

    // Upsert points into the collection
    client.upsert_points_blocking(collection_name, None, points, None).await.unwrap();

    Ok(())
}


async fn query_applicants(client: &QdrantClient, collection_name: &str) -> Result<()> {
    // Run a query on the data
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![60.0, 170.0, 65.0],
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();

    // Print header
    println!("Query Results:"); 
    for result in search_result.result.iter() {
        println!("{:?}\n", result); // Print each result on a new line
    }
    println!("Query completed!"); 
    Ok(())
}


