# Yuwen_IDS721_mini7

## Project Features
1. Ingest data into Vector database
2. Perform queries and aggregations
3. Visualize output


## Getting Started

To get started with Qdrant, follow these steps:

1. **Installation**: Refer to the [installation guide](https://github.com/qdrant/qdrant#installation) to install Qdrant on your system.
2. **Data Ingestion**: Use the Qdrant API or command-line tools to ingest your data into the database. You can import vectors from CSV files, JSON objects, or directly through the API.
3. **Indexing**: Once the data is ingested, create an index and configure the indexing parameters according to your requirements. Qdrant supports different indexing methods, including HNSW and Annoy.
4. **Search**: Utilize the Qdrant API to perform similarity search and retrieve nearest neighbors based on your vectors' similarity.

For more detailed information on how to use Qdrant, refer to the [official documentation](https://qdrant.github.io/qdrant/).


## Steps
### Pull qdrant docker image
 ```docker pull qdrant/qdrant```

### Run qdrant in backgound
```docker run -p 6333:6333 -p 6334:6334 -v $(pwd)/qdrant_storage:/qdrant/storage:z qdrant/qdrant```

![qdrant](imgs/qdrant.png){width=500 px}

### Build functions
Modify ```main.rs``` and ```Cargo.toml``` to perform vector database creation, ingestion and querying result.

qdrant running:

![qdrant](imgs/qdrant_create.png){width=500 px}

### Run Project
``` cargo run```

![query](imgs/query_result.png){width=500 px}